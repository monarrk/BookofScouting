# The Book of Scouting
A book to document hacks, tricks, and discovery for use in FRC.

This book is intended to be concise yet thorough. With that in mind, low level details of each process should be documented, but these details are not necesarily meant to be explained in a human-friendly way. For example, RFCOMM, its usage, and how it's being used will all be discussed, but RFCOMM itself will not be defined.

## Table of Contents
1. [How Do Scouting](#how-do-scouting)
2. [Raspberry Pi](#raspberry-pi)
3. [Bluetooth Hacks](#bluetooth-hacks)
4. [How to Not Use JSON](#how-to-not-use-json)
5. [Making the App](#making-the-app)
    1. [MainActivity](#mainactivity)
    2. [Teams](#teams)
    3. [NewTeam](#newteam)

## How Do Scouting
How to Scouting Good

**Technical Scouting Process**:
The scouting app and server go through this process:
1. Server begins running
   - Broadcasts RFCOMM signal, typically on port 1
2. Client begins running
   - Locates pi using the pi's MAC address
   - Attempts to connect over RFCOMM
3. Client sends empty data byte string
4. Pi sends team data located at ROOT/teamData.json to client, where ROOT is the full path to the directory that contains teamData.json
5. Pi closes connection
6. Client parses team data and offers team selection
   - Human enters match data
7. Client reconnects to pi
8. Client sends match data to the pi
9. Pi closes connection

## Raspberry Pi
**Disable WiFi**:
To disable WiFi, add the following line to ``/boot/config.txt``: ``dtoverlay=pi3-disable-wifi``. An easy command for doing this is ``echo 'dtoverlay=pi3-disable-wifi' >> /boot/config.txt``.

**Bluetooth Packages**:
Bluetooth packages typically used in the past from a system package manager
- bluez-tools
- python3
- python3-pip

Bluetooth packages for python
- pybluez

**Configure systemd**:
The following commands will create and configure systemd services for bluetooth:
```
        cat > /etc/systemd/network/pan0.netdev << EOF
        [NetDev]
        Name=pan0
        Kind=bridge
        EOF

        cat > /etc/systemd/network/pan0.network << EOF
        [Match]
        Name=pan0
        [Network]
        Address=172.20.1.1/24
        DHCPServer=yes
        EOF

        cat > /etc/systemd/system/bt-agent.service << EOF
        [Unit]
        Description=Bluetooth Auth Agent
        [Service]
        ExecStart=/usr/bin/bt-agent -c NoInputNoOutput
        Type=simple
        [Install]
        WantedBy=multi-user.target
        EOF

        cat > /etc/systemd/system/bt-network.service << EOF
        [Unit]
        Description=Bluetooth NEP PAN
        After=pan0.network
        [Service]
        ExecStart=/usr/bin/bt-network -s nap pan-
        Type=simple
        [Install]
        WantedBy=multi-user.target
        EOF
```

Remeber to enable these services by running ``systemctl enable Foo``, where Foo is the service name.

## Bluetooth Hacks
Bluetooth Hacks

**Fix systemd's bluetooth compatibility mode**:
Systemd, the init system for debian-derived distributions of Linux, defaults to running the Bluetooth Daemon (bluetoothd) without compatibility mode on. If you are trying to use RFCOMM (serial over bluetooth), then your programs will fail. To fix this, you need to edit systemd's configuration file for bluetoothd with the flag ``--compat`` added to the ExecStart key. 

Example configuration file:
```
        [Unit]
        Description=Bluetooth service
        Documentation=man:bluetoothd(8)
        ConditionPathIsDirectory=/sys/class/bluetooth
        [Service]
        Type=dbus
        BusName=org.bluez
        ExecStart=/usr/lib/bluetooth/bluetoothd --compat # Notice the flag here
        NotifyAccess=main
        #WatchdogSec=10
        #Restart=on-failure
        CapabilityBoundingSet=CAP_NET_ADMIN CAP_NET_BIND_SERVICE
        LimitNPROC=1
        ProtectHome=true
        ProtectSystem=full
        [Install]
        WantedBy=bluetooth.target
        Alias=dbus-org.bluez.service
```

To test whether or not you have succeeded, you can try running ``sdptool browse local``, assuming ``sdptool`` is installed on your system.

## How to Not Use JSON

**Do not parse JSON as a string**:
Parsing JSON as a string allows for a myriad of errors that could lead to the production of invalid JSON and should be avoided at all costs. Almost every language used by more than 100 people has a library available to parse JSON using types and methods rather than plaintext and you should use them *for sure*.

**Do not do anything before validating your JSON**:
Especially dangerous things, like opening files for writing. If you somehow get invalid JSON and your program blows up, there can be unexpected consequences that you don't know how to fix, which is an unfortunate situation at competitions.

## Making the App
The app has many distinct pages (AppCompatActivities) which are used for the main functionality. Roughly speaking, these are:
- The `MainActivity` or `BeginningScreen` page, which is where the app starts
- The `Teams` page, which is a list of all the teams collected from the pi which can be used to start a new match as the team selected from the list. This should also have functionality to allow adding new teams
- The `NewTeam` activity, which allows you to create and insert a new team into the database
- The `MatchInformation`/`MatchStart` page, which holds the fragments that you can use to collect data
- TODO : are there more?

An extra note: please please please make the package name for the app something anonymous, NOT your personal username. This is a team after all. I (skylar) personally liked the name `com.scouting.scoutapp`, but anything like that should work.

#### Implementation details and "best practices" as of the 2021 season
##### MainActivity
In the `onCreate` method, create a `Button` (you'll need to add this to the corresponding XML layout file in `app/res/layout`). This button should be set to start the `Teams` activity when clicked. This is relatively straightforward, but has some sort of confusing syntax involving `::`, so here's an example in Kotlin:

``` kotlin
// Use R.id.{{button_id}} to access the button from the XML
val newMatch = findViewById<Button>(R.id.newMatchBtn)

// Set behavior for the button
newMatch.setOnClickListener {
	Log.i(tag, "Clicked new match button")
	val teams = Intent(applicationContext, Teams::class.java)
	startActivity(teams)
}
```

This assumes your `Teams` activity is actually a class called `Teams`.

At this point, you should setup the database, and you will likely need to attach that database to a bluetooth class somewhere in your code. The 2021 scouting app does this as so:

``` kotlin
// see the full code at https://github.com/SalineSingularityFRC/ScoutApp2021/blob/master/app/src/main/java/com/scouting/scoutapp/MainActivity.kt#L39

// top level class declaration
private var bluetooth: BluetoothClass = BluetoothClass(this)
var database: Database = Database()

// ...snip...

// inside onCreate()
database.setup(bluetooth)
```

In the `onStart` method, you should setup the bluetooth class (`bluetooth.setup()`) and send the initial empty data over the RFCOMM connection. This looks roughly like this:

``` kotlin
bluetooth.setup()
bluetooth.send("{\"teamData\":[],\"matchData\":[]}")
```

##### Teams
The `Teams` page is slightly more complex as it involves actually interacting with bluetooth and database code. 

In the `onCreate` method, you should set up a `ListView` and a `Button`, for listing the teams and creating a new team respectively. In *my opinion*, the view variable is best stored as an instance variable so you can use it later during `onStart`. 

The `ListView` should be set to a *list layout*, NOT a fragment or activity. That layout will dictate the way that each item in the list is created, the style for which can be edited using the XML editor. Next, you should set the `OnClickListener` for the `ListView` to start the `NewMatch` page. TODO : finish. Here's an example:

``` kotlin
// get the view with its id in the layout
this.view = findViewById(R.id.list_item)
this.view.setOnItemClickListener { adapterView: AdapterView<*>, view1: View, i: Int, l: Long ->
	// start MatchInformation page
}
```

Next, set the `OnClickListener` for the `Button` to start the `NewTeam` page. This is just like the button from the `MainActivity`, and uses that funky `::` syntax again:

``` kotlin
val newTeam = findViewById<Button>(R.id.newTeamButton)
newTeam.setOnClickListener {
	// start NewTeam page
	Log.i(tag, "Clicked 'New Team' button")

	val newTeam = Intent(applicationContext, NewTeam::class.java)
	startActivity(newTeam)
}
```

In the `onResume` method, you're going to access the database and display the contents using an `ArrayAdapter`. 

Firstly, create a `MutableList` of `HashMaps` which hold a `String` key and a `String` value. In kotlin, this looks like `MutableList<HashMap<String, String>>` and is created like this:

``` kotlin
val list = mutableListOf<HashMap<String, String>>()
```

Note that although this has a *type* of a `MutableList`, the variable itself is not mutable. This means that the *elements* of the list can be modified or added, but the variable itself cannot be reassigned. There's no reason we would need to fully reassign the list, so we make it immutable.

Now, we are going to create an `ArrayAdapter`, and more specifically a `SimpleAdapter`. The `SimpleAdapter` takes in a list and renders it cleanly as a `ListView` in the actual app. The `SimpleAdapter` is a flavor of `ArrayAdapter` that looks nice and is easy to use, so we'll use that. We create the adapter like this:

``` kotlin
// create an adapter attached to a layout, NOT a fragment or activity
// the adapter takes a list of hashmaps and presents them in a friendly way on the app
// must use the constructor that takes in a TextView (list_text in this instance)
val arrayAdapter = SimpleAdapter(this,
	list,
	R.layout.listlayout,
	arrayOf("name", "number"), // the keys to use from the map
	intArrayOf(R.id.teamName, R.id.teamNumber)) // the ids on the listlayout to map to the values
```

There are a few things to notice here. Firstly, the `arrayOf("name", "number")` line. This is an array which holds the keys to use to access the members of the `HashMap` in the list. Secondly, the `intArrayOf(R.id.teamName, R.id.teamNumber)`. `teamName` and `teamNumber` are both `TextViews` located in the `listlayout` XML file. These correspond with the indexes of the key array passed in before. In this case the value found at the key `"name"` is displayed in the `teamName` view, and the `"number"` value is displayed in the `teamNumber` view. This pattern is repeated all the way through the list and displays each entry.

The last step is to set the `ListView`'s adapter to our new `ArrayAdapter`. This is easy as pie:

``` kotlin
view.adapter = arrayAdapter
```

Phew. Finally.

Now, for the bluetooth code. You'll have to iterate over each entry in the `Database`'s `teamData` variable as well as each entry in the `tempTeamData` variable. This is best done with a "for in until" pattern, like this:

``` kotlin
for (i in 0 until Database.teamData.length())
```

Inside this loop, create a temporary `HashMap` to hold the team data, which will be appended to the `list`. This can be done something like this:

``` kotlin
val resultsMap = HashMap<String, String>()

resultsMap["name"] = getTeamName(i)
resultsMap["number"] = "${getTeamNumber(i)}"
list.add(resultsMap)
```

Viola! We've now added all the `teamData` in the database to the list. Repeat this block exactly, but with the `tempTeamData` variable instead. Our completed block now looks something like this:

``` kotlin
for (i in 0 until Database.teamData.length()) {
	val resultsMap = HashMap<String, String>()

	resultsMap["name"] = getTeamName(i)
	resultsMap["number"] = "${getTeamNumber(i)}"
	list.add(resultsMap)
}

for (i in 0 until Database.tempTeamData.length()) {
	val resultsMap = HashMap<String, String>()

	resultsMap["name"] = getLocalTeamName(i)
	resultsMap["number"] = "${getLocalTeamNumber(i)}"
	list.add(resultsMap)
}
```

TODO : there's probably a good way to make this repeat less right?

The final step is to inform the `ArrayAdapter` that we've updated the list so that it can render the new data. This is simple:

``` kotlin
arrayAdapter.notifyDataSetChanged()
```

##### NewTeam
The `NewTeam` page allows users to add new teams to the database. It's actually pretty straightforward, and lives entirely in `onCreate`. The trickiest part is sanitizing inputs. For now, we just check for empty teams, but more sanitization may be needed if people find some odd vulnerability (user input is almost *always* vulnerable).

The page contains three elements: two `EditText` elements, one for the team name and one for the team number, and a `Button` to submit the data. 

In the `onCreate` method, create variables attached to each element listed above. Set the button's `OnClickListener` to get the name and number from the `EditText` elements like this:

``` kotlin
val checkTeamName = teamName.getText().toString()
val checkTeamNumber = teamNumber.getText().toString()
```

Note these are called `checkTeamName` and `checkTeamNumber` because we now have to sanitize these strings.

An easy way to sanitize is with *regular expressions* (more commonly called "RegEx"). Our RegEx will be simple, because we are only checking for an empty string, but it allows us to make more complex statements later if we need. Here's a simple example:

``` kotlin
if (checkTeamName.matches(Regex(""))) { }
```

If it's an empty string, this block will run. Here, you should include an alert informing the user that they need to input a name/number and return to restart the process.

Otherwise, run the `Database.makeTeam` function/method, passing in the name and number. An easy way to convert the team number to an actual `Int` is with `Integer.parseInt(checkTeamNumber)`.

